package com.example.employeeparking.exceptions;

public class NotFreeSpotFoundException  extends RuntimeException{
	public NotFreeSpotFoundException(String message) {
		super(message);
	}

}
