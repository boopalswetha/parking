package com.example.employeeparking.controller;

import java.time.LocalDate;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.service.SpotReleaseService;

/**
 * this functionally usage is realseing spot in vip employee
 * @author saikrishna
 * @version 1.0
 *  @since 2020/06/16
 */

@RestController
@RequestMapping("/responses")
public class SpotReleaseController {
	
	
	org.jboss.logging.Logger logger = LoggerFactory.logger(SpotReleaseController.class);

	@Autowired
	SpotReleaseService spotReleaseService;
	
	@PostMapping("/")
	public ResponseEntity<RequestResponseDto> releaseSpot(@RequestParam Long employeeId,@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate startDate,
								@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate endDate) {
		logger.info("checking vip employee only release the slot");
		RequestResponseDto requestResponseDto = new RequestResponseDto();
		requestResponseDto = spotReleaseService.releaseSpot(employeeId,startDate,endDate);
		return new ResponseEntity<>(requestResponseDto,HttpStatus.OK);
	}

}
