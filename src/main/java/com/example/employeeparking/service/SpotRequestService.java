package com.example.employeeparking.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.employeeparking.controller.SpotReleaseController;
import com.example.employeeparking.dto.ReleaseDto;
import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.exceptions.DateNotValid;
import com.example.employeeparking.exceptions.EmployeeNotFoundException;
import com.example.employeeparking.exceptions.EnrollAlreadyDoneException;
import com.example.employeeparking.exceptions.NoFreeSpotsFoundException;
import com.example.employeeparking.exceptions.NotFreeSpotFoundException;
import com.example.employeeparking.exceptions.RequestNotProcessedException;
import com.example.employeeparking.exceptions.SpotRequestNotFoundException;
import com.example.employeeparking.model.Employee;
import com.example.employeeparking.model.FreeSpot;
import com.example.employeeparking.model.SpotRequest;
import com.example.employeeparking.repository.EmployeeRepository;
import com.example.employeeparking.repository.FreeSpotRepository;
import com.example.employeeparking.repository.SpotRequestRepository;

/**
 * this is raise request and normal employee and accept the lottory tokens
 * @author boopal.sivakumar and saikrishna and lashmi chandhani
 * @version 1.0
 * @since 2020/06/16
 */


@Service
public class SpotRequestService {
		
	
	@Autowired
	SpotRequestRepository spotRequestRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;	 
	
	@Autowired
	FreeSpotRepository freeSpotRepository;

	Logger logger = LoggerFactory.getLogger(SpotReleaseController.class);
	
	
	public RequestResponseDto raiseRequest(Long employeeId,LocalDate startDate,LocalDate endDate) {
		RequestResponseDto response = new RequestResponseDto();
		Employee employee = employeeRepository.findById(employeeId).orElseThrow(()->new EmployeeNotFoundException("employee with given id not found"));
		logger.info("checking this employee id present or not");
		LocalDate localDate = LocalDate.now();
		if(employee.getIsVip()==false) {
			logger.debug("normal employee only raise the request");
			if(startDate.compareTo(localDate)>0) {
				logger.debug("checking starting to localDte");
				int startDate1 = startDate.getDayOfMonth();
				int endDate1 = endDate.getDayOfMonth();
				int difference = endDate1-startDate1;
				if(difference<2) {
					logger.debug("thiis employee only 2 days only tekes");
					SpotRequest spotRequest1=spotRequestRepository.findSpotRequestByEmployeeAndDate(employee,startDate);
					SpotRequest spotRequest2=spotRequestRepository.findSpotRequestByEmployeeAndDate(employee,endDate);
					if((spotRequest1==null)&&(spotRequest2==null)) {
						for(LocalDate date = startDate;date.compareTo(endDate)<=0;date = date.plusDays(1)) {
							SpotRequest spotRequest = new SpotRequest();
							spotRequest.setEmployee(employee);
							spotRequest.setDate(date);
							spotRequest.setStatus("not assigned");
							spotRequestRepository.save(spotRequest);
						}
						logger.info("all the information set sucessfully");
						response.setMessage("successfully raised request");
					}
					else {
						logger.error("this request already send");
						throw new EnrollAlreadyDoneException("already raised request");
					}
				}
				else {
					logger.error("normal only raise only 2days");
					response.setMessage("Raising request is not successfull");
					throw new DateNotValid("you can only request for 2 days");
				}
			}
			else {
				logger.error("raising is not sucessfully");
				response.setMessage("Raising request is not successfull");
				throw new DateNotValid("start date should be greater than todays date");
			}
		}
		else {
			logger.error("this raissing request not sucessfull");
			response.setMessage("Raising request is not successfull");
		}
		return response;
	}
	
	
/*	public RequestResponseDto requestProcessing() throws RequestNotProcessedException, SpotRequestNotFoundException, NoFreeSpotsFoundException {
			LocalDate date = LocalDate.now();
			List<SpotRequest> employeeReq = spotRequestRepository.findByDateGreaterThanAndStatus(date, "not assigned");
			if(employeeReq.isEmpty()) {
				throw new SpotRequestNotFoundException("no requests found for spots");
			}
			List<Long> spotrequestIds = employeeReq.stream().map(m -> m.getSpotRequesttId()).collect(Collectors.toList());
			Long spotRequestId = getRandomElement(spotrequestIds);
			SpotRequest spotRequest = spotRequestRepository.findById(spotRequestId).get();
			

			List<FreeSpot> freeSpots = freeSpotRepository.findByDateGreaterThanAndStatus(date, "not assigned");
			if(freeSpots.isEmpty())
			{
				throw new NoFreeSpotsFoundException("free spots not available");
			}
			List<Long> freeSpotIds = freeSpots.stream().map(m -> m.getFreeSpotId()).collect(Collectors.toList());
			Long freeSpotId = getRandomElement(freeSpotIds);
			FreeSpot freeSpot = freeSpotRepository.findById(freeSpotId).get();
			
			if (freeSpot.getDate().equals(spotRequest.getDate())) {
				
				freeSpot.setStatus("assigned");
				spotRequest.setFreeSpot(freeSpot);
				spotRequest.setStatus("assigned");
				freeSpotRepository.save(freeSpot);
				spotRequestRepository.save(spotRequest);

			} else {
				throw new RequestNotProcessedException("request not processed");
			}
			RequestResponseDto responseDto=new RequestResponseDto();
			responseDto.setMessage("requests for parking spots hass been done");
			return responseDto;

		}*/
	/**
	 * this is employeeid to find raise the request and how many times booked that spot 
	 * @param list
	 * @return
	 */

		public Long getRandomElement(List<Long> list) {
			Random rand = new Random();
			return list.get(rand.nextInt(list.size()));
		}
		
		public List<SpotRequest> getByEmployeeId(long employeeId) throws SpotRequestNotFoundException {
			  Employee employee = employeeRepository.findById(employeeId).orElseThrow(()->new EmployeeNotFoundException("Employee with give id not found"));
				List<SpotRequest> spotRequests = spotRequestRepository.findByEmployee(employee);
				if(spotRequests.isEmpty()) {
					logger.debug("this request is not found");
					throw new SpotRequestNotFoundException("request not found");
				}
				else {
					return spotRequests;
				}
				
				
		}
		/**
		 * this is lottory method to allow all the asign dates
		 * @return
		 * @throws RequestNotProcessedException
		 * @throws SpotRequestNotFoundException
		 * @throws NoFreeSpotsFoundException
		 */

		public RequestResponseDto requestProcessing() throws RequestNotProcessedException, SpotRequestNotFoundException, NoFreeSpotsFoundException {
			LocalDate date = LocalDate.now();
			RequestResponseDto requestResponseDto = new RequestResponseDto();
			List<SpotRequest> employeeReq = spotRequestRepository.findByDateGreaterThanAndStatus(date, "not assigned");
			if(employeeReq.isEmpty()) {
				logger.error("no request are spots ");
				throw new SpotRequestNotFoundException("no requests found for spots");
			}
			List<Long> spotrequestIds = employeeReq.stream().map(m -> m.getSpotRequesttId()).collect(Collectors.toList());
			List<FreeSpot> freeSpots = freeSpotRepository.findByDateGreaterThanEqualAndStatus(date, "not assigned");
			if(freeSpots.isEmpty()) {
				logger.debug("this freespot is not available");
				throw new NotFreeSpotFoundException("not Freespot available");
			}
			List<Long> freeSpotIds = freeSpots.stream().map(m -> m.getFreeSpotId()).collect(Collectors.toList());
			for (int i=0; i< freeSpotIds.size();i++) {
				logger.info("chechecking freespot is asigned or not");
				FreeSpot freeSpot = freeSpotRepository.findById(freeSpotIds.get(i)).get();
				for(int  j=0;j<spotrequestIds.size();j++ ) {
					logger.info("chechecking spotrequestIds is asigned or not");
					SpotRequest spotRequest = spotRequestRepository.findById(spotrequestIds.get(j)).get();
					if(freeSpot.getDate().equals(spotRequest.getDate()) && spotRequest.getStatus().equalsIgnoreCase("Not be assigned")) {
						freeSpot.setStatus("assigned");
						spotRequest.setFreeSpot(freeSpot);
						spotRequest.setStatus("assigned");
						freeSpotRepository.save(freeSpot);
						spotRequestRepository.save(spotRequest);
						logger.info("spotrequest save sucessfully");
					}
				}
				logger.debug("all the records save succesfully");
				requestResponseDto.setMessage("Assigned Successfully completed");
				//return  requestResponseDto;
			}
			return  requestResponseDto;
			
		}
}
		
		/*public ReleaseDto saveParking() {
			LocalDate date = LocalDate.now();
			List<SpotRequest> employeeReq = spotRequestRepository.findByStartDateGreaterThanEqualAndStatus(date,
					"not assigned");
			
			List<Long> spotrequestIds = employeeReq.stream().map(m -> m.getSpotRequesttId()).collect(Collectors.toList());
			
			List<FreeSpot> freeSpots = freeSpotRepository.findByDateGreaterThanEqualAndStatus(date, "not assigned");	
			
			List<Long> freeSpotIds = freeSpots.stream().map(spId-> spId.getFreeSpotId()).collect(Collectors.toList());
					
			
			for (Long spotrequestId : spotrequestIds) {
				SpotRequest spotRequest = spotRequestRepository.findById(spotrequestId).get();
				
				for (Long freeSpotId : freeSpotIds) {
					FreeSpot freeSpot = freeSpotRepository.findById(freeSpotId).get();
					
					if (freeSpot.getDate().equals(spotRequest.getDate())&&freeSpot.getStatus().equalsIgnoreCase("not assigned")&&spotRequest.getStatus().equalsIgnoreCase("not assigned")) {
						freeSpot.setStatus("assigned");
						spotRequest.setFreeSpot(freeSpot);
						spotRequest.setStatus("assigned");
						freeSpotRepository.save(freeSpot);
						spotRequestRepository.save(spotRequest);
						
						}
				}
	        }

			ReleaseDto releaseDto = new ReleaseDto();
			releaseDto.setMessage("Assigned sucessfullly");
			
			return releaseDto;


	}*/
	
