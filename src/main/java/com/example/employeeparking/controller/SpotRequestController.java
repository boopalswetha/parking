package com.example.employeeparking.controller;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import com.example.employeeparking.dto.ReleaseDto;
import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.exceptions.NoFreeSpotsFoundException;
import com.example.employeeparking.exceptions.RequestNotProcessedException;
import com.example.employeeparking.exceptions.SpotRequestNotFoundException;
import com.example.employeeparking.model.SpotRequest;
import com.example.employeeparking.service.SpotRequestService;

/**
 * this usecase is lottery ticket and normal employee releasing spotreuest
 * @author boopal.sivakumar and saikrishna
 * @version 1.0
 * @since 2020/06/16
 *
 */

@RestController
@RequestMapping("/requests")
public class SpotRequestController {
	
	@Autowired
	SpotRequestService spotRequestService;
	
	Logger logger = LoggerFactory.getLogger(SpotReleaseController.class);

	/**
	 * this is normal raiserquest normal employeee
	 * @param employeeId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	
	@PostMapping("/")
	public ResponseEntity<RequestResponseDto> raiseRequest(@RequestParam Long employeeId,@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate startDate,
								@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate endDate) {
		RequestResponseDto requestResponseDto = new RequestResponseDto();
		requestResponseDto = spotRequestService.raiseRequest(employeeId,startDate,endDate);
		logger.info("non vip employee request raise");
		return new ResponseEntity<>(requestResponseDto,HttpStatus.OK);
	}
	/**
	 * this lottery request processing system
	 * @return
	 * @throws RequestNotProcessedException
	 * @throws SpotRequestNotFoundException
	 * @throws NoFreeSpotsFoundException
	 */
	
	
	@GetMapping("/LotterySystem")
	public ResponseEntity<RequestResponseDto> requestProcessingByLotterySystem() throws RequestNotProcessedException, SpotRequestNotFoundException, NoFreeSpotsFoundException {
		RequestResponseDto message =spotRequestService.requestProcessing();
		logger.info("Accept the lottory tokens");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	/**
	 * this employeeid to how many spots get history of information
	 * @param employeeId
	 * @return
	 * @throws SpotRequestNotFoundException
	 */
	
	
	@GetMapping("/search")
	public ResponseEntity<List<SpotRequest>> getEmployeeById(@RequestParam long  employeeId) throws SpotRequestNotFoundException {
		List<SpotRequest> EmployeeParkingLot = spotRequestService.getByEmployeeId(employeeId);
		logger.info("Get the employeeeid history");
		return new ResponseEntity<>(EmployeeParkingLot, HttpStatus.OK);
	}
	
	/*@GetMapping("/lottnew")
	public ResponseEntity<ReleaseDto>lotterySystem1(){
		ReleaseDto releaseDto=	spotRequestService.saveParking();
		return new ResponseEntity<>(releaseDto,HttpStatus.OK);
	}*/
	


}
